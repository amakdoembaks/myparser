package nl.ksyos.hl7.parser.controller;

import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.parser.DefaultXMLParser;
import ca.uhn.hl7v2.parser.PipeParser;
import ca.uhn.hl7v2.parser.XMLParser;
import nl.ksyos.hl7.parser.exception.ResourceNotFoundException;
import nl.ksyos.hl7.parser.model.Patient;
import nl.ksyos.hl7.parser.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/api")
public class PatientController {

    @Autowired
    PatientRepository patientRepository;

    // Get All Patients
    @GetMapping("/patients")
    @RequestMapping(value="/patients", method= RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Patient> getAllPatients(){

        return patientRepository.findAll();
    }

    // Get Patient
    @GetMapping("/patient/{id}")
    public Patient getPatienById(@PathVariable(value = "id") UUID id){
        return patientRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Patient", "id", id));
    }

    // Parse Message
    @PostMapping("/parse")
    public String parseHL7Message(@Valid @RequestBody String message){

        String result = "-leeg-";
        //message = "MSH|^~\\&|SENDING_APPLICATION|SENDING_FACILITY|RECEIVING_APPLICATION|RECEIVING_FACILITY|20110614075841||ACK|1407511|P|2.4||||||\r\nMSA|AA|1407511|Success||";
        //message = "MSH|^~\\&|Care2U|Care2U-HKW|KSYOS|KSYOS TeleMedisch Centrum|20190305210119||OML^O21|20190305210101962|P|2.4|||AL|NE\r\nPID|1||999928600^^^NLMINBIZ^NNNLD||van Beek-Bergen&van&Beek&&Bergen^^^^^^L||20001003|M|||stationsstraat 214&stationsstraat&214^^Leiden^^2317DA^NL^H||^PRN^PH\r\nORC|NW|2019-000-310|||||^^^^20190430|||||94123457^Uisarts &&Uisarts^H.^^^^^01^VEKTIS|||||HKW\r\nOBR|1|2019-000-310||FUN01^Fundusfoto^NHG|||||||L||||||||||||||||||||DM\r\nNTE|1||Eerstelijns DBC|^DBC";
        PipeParser ourPipeParser = new PipeParser();

        System.out.println(message);

        try{
            Message hl7Message = ourPipeParser.parse(message);

            System.out.println("HERE 1");
            result =  hl7Message.printStructure();

            System.out.println("HERE 2");
            XMLParser xmlParser = new DefaultXMLParser();

            System.out.println(xmlParser.encode(hl7Message));
            System.out.println("HERE 3");

            System.out.println(result);
        }catch(Exception e){
            System.out.println(e);
            throw new ResourceNotFoundException("Parse", "id", null);
        }

        return result;
    }
}
